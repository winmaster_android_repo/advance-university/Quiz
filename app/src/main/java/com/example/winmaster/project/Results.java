package com.example.winmaster.project;

import android.arch.persistence.room.ColumnInfo;
import android.arch.persistence.room.Entity;
import android.arch.persistence.room.PrimaryKey;
import android.arch.persistence.room.TypeConverters;

import java.sql.Date;
import java.sql.Timestamp;

/**
 * Created by Katarzyna on 2018-03-26.
 */

@Entity
public class Results
{
    @PrimaryKey(autoGenerate = true)
    private int id;

    @ColumnInfo(name = "result")
    private int result;

    public int getId()
    {
        return id;
    }

    public void setId(int id)
    {
        this.id = id;
    }


    public int getResult()
    {
        return result;
    }

    public void setResult(int result)
    {
        this.result = result;
    }
}
