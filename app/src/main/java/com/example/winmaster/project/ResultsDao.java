package com.example.winmaster.project;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.Query;

/**
 * Created by Katarzyna on 2018-03-26.
 */

@Dao
public interface ResultsDao
{
    @Query("Select Count(*) From Results")
    int getAmountOfGames();

    @Query("SELECT AVG(result) FROM Results")
    double getAverageResult();

    @Query("SELECT MAX(result) FROM Results")
    int getMaxResult();

    @Query("SELECT MIN(result) FROM Results")
    int getMinResult();

    @Insert
    void insert(Results result);

    @Query("SELECT * FROM Results")
    Results[] selectAll();

}
