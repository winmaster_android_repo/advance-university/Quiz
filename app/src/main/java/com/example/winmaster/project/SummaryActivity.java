package com.example.winmaster.project;

import android.app.Application;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

public class SummaryActivity extends AppCompatActivity
{

    TextView textViewCurrentResult;
    TextView textViewAmountGames;
    TextView textViewMaxResult;
    TextView textViewMinResult;
    TextView textViewAvgResult;
    ImageView imageView;
    public ResultsDao resultsDao;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_summary);
        this.setTitle("Podsumowanie");

        resultsDao = AppDatabase.getDatabase(getApplicationContext()).resultsDao();
        textViewCurrentResult = findViewById(R.id.textViewCurrentResult);
        textViewAmountGames = findViewById(R.id.textViewAmountGames);
        textViewMaxResult = findViewById(R.id.textViewMaxResult);
        textViewMinResult = findViewById(R.id.textViewMinResult);
        textViewAvgResult = findViewById(R.id.textViewAvgResult);
        imageView = findViewById(R.id.imageView);

        Intent intent = getIntent();
        int result = intent.getIntExtra("result", -1);
        int amountQuestions = intent.getIntExtra("amountQuestions", -1);

        textViewCurrentResult.setText("Wynik: " + result+ "/"+amountQuestions);

        textViewAmountGames.setText(getResources().getString(R.string.AmountGames) + "\t\t" + resultsDao.getAmountOfGames());
        textViewMaxResult.setText(getResources().getString(R.string.maxResult) + "\t\t" + resultsDao.getMaxResult());
        textViewMinResult.setText(getResources().getString(R.string.minResult) + "\t\t" + resultsDao.getMinResult());
        textViewAvgResult.setText(String.format("%s %s %1.2f",getResources().getString(R.string.avgResult), "\t\t",resultsDao.getAverageResult()));

        if(result == amountQuestions || result == amountQuestions -1)
        {
            imageView.setImageDrawable(getResources().getDrawable(R.drawable.smile));
        }
        if(result == 2 || result == 3)
        {
            imageView.setImageDrawable(getResources().getDrawable(R.drawable.neutral));
        }
        if(result == 0 || result == 1)
        {
            imageView.setImageDrawable(getResources().getDrawable(R.drawable.sad));
        }

    }

    @Override
    public void onBackPressed()
    {
        Intent i = getBaseContext().getPackageManager().getLaunchIntentForPackage( getBaseContext().getPackageName());
        i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivity(i);
    }
}
