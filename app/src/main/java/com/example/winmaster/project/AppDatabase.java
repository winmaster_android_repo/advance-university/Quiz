package com.example.winmaster.project;

import android.arch.persistence.room.Database;
import android.arch.persistence.room.Room;
import android.arch.persistence.room.RoomDatabase;
import android.arch.persistence.room.TypeConverters;
import android.content.Context;

/**
 * Created by Katarzyna on 2018-03-26.
 */

@Database(entities = {Results.class}, version = 1)
public abstract class AppDatabase extends RoomDatabase
{
    private static AppDatabase instance;

    public static AppDatabase getDatabase(Context context)
    {
        if (instance == null)
            instance = Room.databaseBuilder(context, AppDatabase.class, "quiz-results").allowMainThreadQueries().build();
        return instance;
    }

    public abstract ResultsDao resultsDao();
}


