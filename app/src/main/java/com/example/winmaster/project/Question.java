package com.example.winmaster.project;
import android.graphics.drawable.Drawable;
import android.media.Image;

import java.util.ArrayList;

/**
 * Created by winmaster on 16.03.2018.
 */

public class Question
{
    public String questionText;
    public Drawable questionImage;
    public ArrayList<Answer> answers;
    public int amountOfAnswers;

    public Question(String _questionText, Drawable _img, int _amount)
    {
        questionText = _questionText;
        questionImage = _img;
        amountOfAnswers = _amount;
        setAmountOfAnswers(_amount);
    }

    public void setAmountOfAnswers(int number)
    {
        answers = new ArrayList<>(number);
    }

    public void addAnswer(String variant, boolean check)
    {
        if(answers.size()<amountOfAnswers)
        {
            Answer answer = new Answer(variant, check);

            answers.add(answer);
        }
    }

    public class Answer
    {
        public String variant; //text
        public boolean check; //true if correct, false if dont

        public Answer(String _variant, boolean _check)
        {
            variant = _variant;
            check = _check;
        }

        public void setVariant(String variant)
        {
            this.variant = variant;
        }

        public void setCheck(boolean check)
        {
            this.check = check;
        }

        public String getVariant()
        {
            return variant;
        }

        public boolean isCheck()
        {
            return check;
        }
    }
}


