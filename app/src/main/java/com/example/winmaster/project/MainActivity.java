package com.example.winmaster.project;

import android.arch.persistence.room.Room;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity
{
    public ArrayList<Question> questions = new ArrayList<Question>();

    public ResultsDao resultsDao;
    public TextView textViewQuestion;
    public ImageView imageViewPhoto;
    public RadioGroup radioGroupQuestions;
    public RadioButton radioButtonFirst;
    public RadioButton radioButtonSecond;
    public RadioButton radioButtonThird;
    public int amountQuestions;
    public int questionCounter;
    public int userResult;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        resultsDao = AppDatabase.getDatabase(getApplicationContext()).resultsDao();

        textViewQuestion = findViewById(R.id.textViewQuestion);
        imageViewPhoto = findViewById(R.id.imageViewPhoto);
        radioGroupQuestions = findViewById(R.id.radioGroupQuestions);
        radioButtonFirst = findViewById(R.id.radioButtonFirst);
        radioButtonSecond = findViewById(R.id.radioButtonSecond);
        radioButtonThird = findViewById(R.id.radioButtonThird);

        setQuestionsToArray();
        amountQuestions = questions.size();
        questionCounter = 0;
        userResult = 0;
        getNextQuestion();
    }

    public void getNextQuestion()
    {
        if(validateAnswerForQuestion()) userResult++;

        if(questionCounter < amountQuestions)
        {
            textViewQuestion.setText(questions.get(questionCounter).questionText);
            imageViewPhoto.setImageDrawable(questions.get(questionCounter).questionImage);
            radioButtonFirst.setText(questions.get(questionCounter).answers.get(0).getVariant());
            radioButtonSecond.setText(questions.get(questionCounter).answers.get(1).getVariant());
            radioButtonThird.setText(questions.get(questionCounter).answers.get(2).getVariant());
            this.setTitle(getString(R.string.app_name) + " pytanie nr " + (questionCounter+1));

            radioGroupQuestions.clearCheck();
            questionCounter++;
        }
        else
        {
            Results results = new Results();
            results.setResult(userResult);
            resultsDao.insert(results);

            Toast.makeText(this.getApplicationContext(), "Wynik: " + userResult+ "/"+amountQuestions, Toast.LENGTH_SHORT).show();
            Intent intent = new Intent(this, SummaryActivity.class);
            intent.putExtra("result", userResult);
            intent.putExtra("amountQuestions", amountQuestions);
            startActivity(intent);
        }
    }

    public void getPreviousQuestion()
    {
        if(questionCounter > 0)
        {
            questionCounter--;

            textViewQuestion.setText(questions.get(questionCounter).questionText);
            imageViewPhoto.setImageDrawable(questions.get(questionCounter).questionImage);
            radioButtonFirst.setText(questions.get(questionCounter).answers.get(0).getVariant());
            radioButtonSecond.setText(questions.get(questionCounter).answers.get(1).getVariant());
            radioButtonThird.setText(questions.get(questionCounter).answers.get(2).getVariant());
            this.setTitle(getString(R.string.app_name) + " pytanie nr " + (questionCounter+1));
            radioGroupQuestions.clearCheck();
        }
    }


    public boolean validateAnswerForQuestion()
    {
        int checkedID = radioGroupQuestions.getCheckedRadioButtonId();

        switch(checkedID)
        {
            case R.id.radioButtonFirst:
            {
                return questions.get(questionCounter-1).answers.get(0).check;
            }
            case R.id.radioButtonSecond:
            {
                return questions.get(questionCounter-1).answers.get(1).check;
            }
            case R.id.radioButtonThird:
            {
                return questions.get(questionCounter-1).answers.get(2).check;
            }
            default:
            {
                return  false;
            }
        }
    }

    public void setQuestionsToArray()
    {
        Question question1 = new Question("W którym roku była bitwa pod Grunwaldem?", getDrawable(R.drawable.grunwald), 3);
        question1.addAnswer("1383", false);
        question1.addAnswer("1410", true);
        question1.addAnswer("1525", false);

        Question question2 = new Question("Po wodzie pływa i kaczka się nazywa, co to?", getDrawable(R.drawable.duck), 3);
        question2.addAnswer("Jeleń", false);
        question2.addAnswer("Aligator", false);
        question2.addAnswer("Kaczka", true);

        Question question3 = new Question("Z czego zrobiony jest deser Tiramisu?", getDrawable(R.drawable.tiramisu), 3);
        question3.addAnswer("Z mleczka skondensowanego", false);
        question3.addAnswer("Z mascarpone", true);
        question3.addAnswer("Z emulgatora i lecytyny", false);

        Question question4 = new Question("Wybierz prawidłową odpowiedź:", getDrawable(R.drawable.correct), 3);
        question4.addAnswer("Zła odpowiedź", false);
        question4.addAnswer("Jeszcze jedna zła odpowiedź", false);
        question4.addAnswer("Prawidłowa odpowiedź", true);

        Question question5 = new Question("Czy ten test był trudny?", getDrawable(R.drawable.student) , 3);
        question5.addAnswer("Nie", false);
        question5.addAnswer("Raczej tak", false);
        question5.addAnswer("Wybitny austryjacki akwarelista", true);

        questions.add(question1);
        questions.add(question2);
        questions.add(question3);
        questions.add(question4);
        questions.add(question5);
    }

    public void goNext(View view)
    {
        getNextQuestion();
    }

    public void goBack(View view)
    {
        getPreviousQuestion();
    }
}
